﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HopperSpawner : MonoBehaviour
{

    public GameObject eddy;
    public float hpawnrate;
    float timer;
    // Start is called before the first frame update
    void Start()
    {
        timer = 0.0f;
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;

        if (timer > hpawnrate)
        {
            timer = 0.0f;
            var hopblockinstance = Instantiate(eddy, new Vector3(Random.Range(-20f, 20f), 10f, Random.Range(-20f, 20f)), Random.rotation);
            //t = pickupblockinstance.GetComponent<Transform>();
            Destroy(hopblockinstance, 20.0f);
            //make a square of variable length
        }
    }
}