﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class PlayerController : MonoBehaviour
{
    public float speed;
    int count;
    Rigidbody rb;
    Transform t;
    public Text countText;
    public Text winText;
    // Start is called before the first frame update



    void Start()
    {
        rb = GetComponent<Rigidbody>();
        t = GetComponent<Transform>();
        count = 0;
        SetCountText();
        winText.text = "";
        countText.text = "Count: " + count.ToString();
    }

    
    void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);

        rb.AddForce(movement * speed);
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "Pick Up")
        {
            other.gameObject.SetActive(false);
            count = count + 1;
            SetCountText();
        }
        if (other.gameObject.tag == "Edible")
        {
            other.gameObject.SetActive(false);
            t.localScale += new Vector3(.3f, .3f, .3f);
        }
    }

    void SetCountText()
    {
        //print(countText.text);
        countText.text = "Count:" + count.ToString();
        if (count >= 12)
        {
            winText.text = "You Win!";
        }
    }
}
