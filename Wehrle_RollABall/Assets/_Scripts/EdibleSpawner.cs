﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EdibleSpawner : MonoBehaviour
{

    public GameObject eddy;
    public float epawnrate;
    float timer;
    //Transform t;
    // Start is called before the first frame update
    void Start()
    {
        timer = 0.0f;
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;

        if (timer > epawnrate)
        {
            timer = 0.0f;
            var edblockinstance = Instantiate(eddy, new Vector3(Random.Range(-20f, 20f), 10f, Random.Range(-20f, 20f)), Random.rotation);
            //t = pickupblockinstance.GetComponent<Transform>();
            Destroy(edblockinstance, 20.0f);
            //make a square of variable length
        }
    }
}