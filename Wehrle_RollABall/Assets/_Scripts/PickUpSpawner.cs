﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUpSpawner : MonoBehaviour
{

    public GameObject pickup;
    public float pspawnrate;
    float timer;
    Transform t;
    // Start is called before the first frame update
    void Start()
    {
        timer = 0.0f;
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;

        if (timer > pspawnrate)
        {
            timer = 0.0f;
            var pickupblockinstance = Instantiate(pickup, new Vector3(Random.Range(-20f, 20f), 10f, Random.Range(-20f, 20f)), Random.rotation);
            //t = pickupblockinstance.GetComponent<Transform>();
            Destroy(pickupblockinstance, 25f);
            //make a square of variable length
        }
    }
}