﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillBlockSpawner : MonoBehaviour
{

    public GameObject killblock;
    public float spawnrate;
    float timer;
    Transform t;
    // Start is called before the first frame update
    void Start()
    {
        timer = 0.0f;
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;

        if (timer > spawnrate)
        {
            float scale = Random.Range(0.5f, 3);
            timer = 0.0f;
            var killblockinstance = Instantiate(killblock, new Vector3(Random.Range(-20f, 20f), 10f, Random.Range(-20f, 20f)),Random.rotation);
            t = killblockinstance.GetComponent<Transform>();
            //make a square of variable length
            t.localScale = new Vector3(scale,scale,scale);
            Destroy(killblockinstance, 15.0f);
        }
    }
}
