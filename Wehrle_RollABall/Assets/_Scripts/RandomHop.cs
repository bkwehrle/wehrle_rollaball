﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomHop : MonoBehaviour
{
    // Start is called before the first frame update
    public float jumprate;
    public float power;
    Rigidbody r;
    float timer;
    
    void Start()
    {
        r = GetComponent<Rigidbody>();
        timer = 0.0f;
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
        if (timer > jumprate)
        {
            r.AddForce(Random.insideUnitSphere*power,ForceMode.Impulse);
            timer = 0.0f;
        }
    }
}
